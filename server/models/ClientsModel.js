const Sequelize = require('sequelize');
const sequelize = require('../utils/databse');

const Clients = sequelize.define('Clients', {
	CID: {
		type: Sequelize.INTEGER(11),
		autoIncrement: true,
		allowNull: false,
		primaryKey: true
	},
	Cname: {
		type: Sequelize.STRING,
		allowNull: false
	},
	Caddress: {
		type: Sequelize.STRING,
		allowNull: false
	},
	Cphone: {
		type: Sequelize.STRING,
		allowNull: true
	}
});
module.exports = Clients;
