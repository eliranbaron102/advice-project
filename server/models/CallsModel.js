const Sequelize = require('sequelize');
const sequelize = require('../utils/databse');

const Calls = sequelize.define('Calls', {
	CallID: {
		type: Sequelize.INTEGER(11),
		autoIncrement: true,
		allowNull: false,
		primaryKey: true
	},
	TechID: {
		type: Sequelize.INTEGER(11),
		allowNull: false
	},
	CreateDate: {
		type: Sequelize.DATE,
		allowNull: false
	},
	CallStartTime: {
		type: Sequelize.DATE,
		allowNull: false
	},
	CallEndTime: {
		type: Sequelize.DATE,
		allowNull: false
	},
	CID: {
		type: Sequelize.STRING,
		allowNull: false
	}
});
module.exports = Calls;
