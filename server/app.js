const express = require('express');
const app = express();
const port = 5000;
var path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
const Calls = require('./models/CallsModel');
const Clients = require('./models/ClientsModel');
const Sequelize = require('sequelize');
const sequelize = require('./utils/databse');
const Op = Sequelize.Op;

app.use(
	bodyParser.urlencoded({
		extended: true
	})
);
app.use(bodyParser.json());

var corsOptions = {
	origin: '*',
	optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204.
};

app.use(cors(corsOptions));

const CallsRoute = require('./routes/CallsRoute');
app.use(CallsRoute);

const ClientRoute = require('./routes/ClientsRoute');
app.use(ClientRoute);

sequelize
	.sync()
	.then((result) => {
		app.listen(port);
	})
	.catch((err) => {
		console.log(err);
	});
