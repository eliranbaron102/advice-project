const con = require('../utils/databse');
const Calls = require('../models/CallsModel');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const SendCalls = require('../utils/returnObToClient');

exports.getCalls = async (req, res, next) => {
	await Calls.findAll()
		.then((Calls) => {
			res.send(SendCalls(Calls, null, 1));
		})
		.catch((err) => {
			res.send(SendCalls(null, err, 0));
		});
};

exports.insertCalls = async (req, res, next) => {
	let calls = req.body;

	await Calls.create(calls)
		.then((Calls) => {
			res.send(SendCalls(Calls, null, 1));
		})
		.catch((err) => {
			res.send(SendCalls(null, err, 0));
		});
};

exports.deleteCalls = async (req, res, next) => {
	let CallID = req.query.CallID;

	await Calls.destroy({
		where: {
			CallID: CallID
		}
	})
		.then((Calls) => {
			res.send(SendMessage(Calls, null, 1));
		})
		.catch((err) => {
			res.send(SendMessage(null, err, 0));
		});
};
