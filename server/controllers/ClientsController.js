const con = require('../utils/databse');
const Clients = require('../models/ClientsModel');

const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const SendClients = require('../utils/returnObToClient');

exports.getAllClients = async (req, res, next) => {
	await Clients.findAll()
		.then((Clients) => {
			res.send(SendClients(Clients, null, 1));
		})
		.catch((err) => {
			res.send(SendClients(null, err, 0));
		});
};

// exports.getSingleProduct = async (req, res, next) => {
// 	let id = req.query.id;

// 	await Products.findAll({
// 		where: {
// 			id: id
// 		}
// 	})
// 		.then((Products) => {
// 			res.send(SendMessage(Products, null, 1));
// 		})
// 		.catch((err) => {
// 			res.send(SendMessage(null, err, 0));
// 		});
// };

// exports.getProductsByCatId = async (req, res, next) => {
// 	let Catid = req.query.Catid;

// 	await Products.findAll({
// 		where: {
// 			categoryId: Catid
// 		}
// 	})
// 		.then((Products) => {
// 			res.send(SendMessage(Products, null, 1));
// 		})
// 		.catch((err) => {
// 			res.send(SendMessage(null, err, 0));
// 		});
// };

exports.insertClients = async (req, res, next) => {
	let clients = req.body;
	await Clients.create(clients)
		.then((clients) => {
			res.send(SendClients(clients, null, 1));
		})
		.catch((err) => {
			res.send(SendClients(null, err, 0));
		});
};

// exports.findProductByName = async (req, res, next) => {
// 	let name = req.query.name;
// 	await Products.findAll({
// 		where: {
// 			name: {
// 				[Op.like]: `%${name}%`
// 			}
// 		}
// 	})
// 		.then((Products) => {
// 			res.send(SendMessage(Products, null, 1));
// 		})
// 		.catch((err) => {
// 			res.send(SendMessage(null, err, 0));
// 		});
// };

exports.deleteClient = async (req, res, next) => {
	let id = req.query.id;

	await Clients.destroy({
		where: {
			CID: id
		}
	})
		.then((Clients) => {
			res.send(SendClients(Clients, null, 1));
		})
		.catch((err) => {
			res.send(SendClients(null, err, 0));
		});
};

// exports.updateProduct = async (req, res, next) => {
// 	await Products.update(req.body, {
// 		where: {
// 			id: Number(req.body.id)
// 		}
// 	})
// 		.then((Products) => {
// 			res.send(SendMessage(Products, null, 1));
// 		})
// 		.catch((err) => {
// 			res.send(SendMessage(null, err, 0));
// 		});
// };
