const express = require('express');
const router = express.Router();
const ClientssController = require('../controllers/ClientsController');
var multer = require('multer');

router.get('/getAllClients', ClientssController.getAllClients);

router.post('/insertClients', ClientssController.insertClients);

// router.get('/deleteClients', ClientssController.deleteClients);

module.exports = router;
