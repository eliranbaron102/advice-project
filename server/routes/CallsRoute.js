const express = require('express');
const router = express.Router();
const CallsController = require('../controllers/CallsController');

router.get('/getCalls', CallsController.getCalls);

router.post('/insertCalls', CallsController.insertCalls);

router.get('/deleteCalls', CallsController.deleteCalls);

module.exports = router;
