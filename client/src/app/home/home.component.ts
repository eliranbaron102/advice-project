import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: [ './home.component.css' ]
})
export class HomeComponent implements OnInit {
	calls: any;
	constructor(public api: ApiService) {
		// this.getAllCalls();
	}
	async getAllCalls() {
		let calls = await this.api.getAllCalls();
		this.calls = calls['data'];
		console.log('calls : ', this.calls);
	}
	ngOnInit(): void {}
}
