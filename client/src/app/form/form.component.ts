import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
})
export class FormComponent implements OnInit {
  calls: any;
  clients: any;
  id: string;
  date: string;
  url = this.api.baseURL;
  status: boolean;
  constructor(public api: ApiService) {
    console.log('here');
    this.getAllCalls();
    this.getAllClaints();
  }
  async getAllCalls() {
    let calls = await this.api.getAllCalls();
    this.calls = calls['data'];
    console.log('calls : ', this.calls);
    this.getDate();
  }
  async getAllClaints() {
    let clients = await this.api.getAllClaints();
    this.clients = clients['data'];
    console.log('clients : ', this.clients);
  }
  onChange($event, type) {
    this.id = type;
  }

  getDate() {
    this.date = this.calls[0]['CreateDate'];
    console.log(this.date);
  }

  checkBoxChange($event) {
    this.status = !this.status;
    console.log(this.status);
  }
  ngOnInit(): void {}
}
