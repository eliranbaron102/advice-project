import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'users',
})
export class UsersPipe implements PipeTransform {
  transform(arr, search: string): any {
    console.log(search);
    let filterArray = [];

    arr.map((item) => {
      if (item['CID'] == search) {
        filterArray.push(item);
      }
    });
    console.log(filterArray);
    return filterArray;
  }
}
