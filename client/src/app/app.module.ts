import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormComponent } from './form/form.component';
import { UsersPipe } from './users.pipe';

@NgModule({
	declarations: [ AppComponent, HomeComponent, FormComponent, UsersPipe ],
	imports: [ BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, NgbModule ],
	providers: [],
	bootstrap: [ AppComponent ]
})
export class AppModule {}
