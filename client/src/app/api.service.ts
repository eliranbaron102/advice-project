import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public baseURL = 'http://www.localhost:5000/';
  constructor(private http: HttpClient) {}
  getAllCalls() {
    return new Promise(async (resolve, reject) => {
      try {
        await this.http.get(this.baseURL + 'getCalls').subscribe((data) => {
          resolve(data);
        });
      } catch (err) {
        console.log(err);
      }
    });
  }
  getAllClaints() {
    return new Promise(async (resolve, reject) => {
      try {
        await this.http
          .get(this.baseURL + 'getAllClients')
          .subscribe((data) => {
            resolve(data);
          });
      } catch (err) {
        console.log(err);
      }
    });
  }
  insertCalls(order) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.http
          .post(this.baseURL + 'insertCalls', order)
          .subscribe((data) => {
            resolve(data);
          });
      } catch (err) {
        console.log(err);
      }
    });
  }
  deleteCalls(id) {
    return new Promise(async (resolve, reject) => {
      try {
        await this.http
          .get(this.baseURL + 'deleteCalls?' + 'id=' + id)
          .subscribe((data) => {
            resolve(data);
          });
      } catch (err) {
        console.log(err);
      }
    });
  }
}
